package com.example.bilocan.tictactoe;

import android.app.Activity;
import android.nfc.Tag;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import java.util.*;

public class MainActivity extends Activity {
    private int player = 1;   //Player number 1-2
    private HashSet<Integer> key_active_elements_player_1 = null;
    private HashSet<Integer> key_active_elements_player_2 = null;
    private ArrayList<TicTacToe> ticTacToes = null;
    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViewList();
    }

    /**
     * initializes used elements
     * ArrayList contains ID of each ImageView elements and number sorted from 1 to 9
     * Each ImageView element implements OnClickListener with onClick,
     * to change picture and check if user won
     *
     * @author bilal
     */

    private void initViewList() {
        key_active_elements_player_1 = new HashSet<>();
        key_active_elements_player_2 = new HashSet<>();
        ticTacToes = new ArrayList<>();

        //Add  ticTacToes Elements to ArrayList
        ticTacToes.add(new TicTacToe( (ImageView) findViewById(R.id.boxOne), 1) );
        ticTacToes.add(new TicTacToe( (ImageView) findViewById(R.id.boxTwo),2) );
        ticTacToes.add(new TicTacToe( (ImageView) findViewById(R.id.boxThree), 3) );
        ticTacToes.add(new TicTacToe( (ImageView) findViewById(R.id.boxFour), 4) );
        ticTacToes.add(new TicTacToe( (ImageView) findViewById(R.id.boxFive), 5) );
        ticTacToes.add(new TicTacToe( (ImageView) findViewById(R.id.boxSix), 6) );
        ticTacToes.add(new TicTacToe( (ImageView) findViewById(R.id.boxSeven), 7) );
        ticTacToes.add(new TicTacToe( (ImageView) findViewById(R.id.boxEight), 8) );
        ticTacToes.add(new TicTacToe( (ImageView) findViewById(R.id.boxNine), 9) );

        //every element has OnClickListener for changing state
        for(final TicTacToe iv : ticTacToes)
            iv.getImageView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    boolean won = false;

                    //ImageView already pressed -> no reaction
                    if(iv.isPressed() ) { return; }

                    //check and act input for selected player
                    //player one is selected
                    if(player == 1) {
                        iv.setPressed(true);
                        iv.getImageView().setImageResource(R.drawable.tictacx);

                        //store key from pressed by player 1 from board
                        key_active_elements_player_1.add(iv.getBoard_number());

                        //calculate path to win
                        won = checkWinningPath();

                        //check if player 1 won the match
                        if(won) {
                            //TODO show popup and end game
                        }

                        //if not won, switch player
                        player = 2;
                    }
                    //player two is selected
                    else {
                        iv.setPressed(true);
                        iv.getImageView().setImageResource(R.drawable.tictaco);

                        //store key from pressed by player from board
                        key_active_elements_player_2.add(iv.getBoard_number());

                        //calculate path to win
                        won = checkWinningPath();

                        //check if player 2 won the match
                        if(won) {
                            //TODO show popup and end game
                        }

                        //if not won, switch player
                        player = 1;
                    }
                }
            });
        }

    /**
     *  Method analyses pressed buttons of each user and checks if pressed path leads to win the match
     *
     *  @return returns true if a player won, else false if no one won
     *  @author bilal
     */
    private boolean checkWinningPath() {
        boolean won = false;

        //get pressed buttons of player 1 and check if path leads to win
        if (key_active_elements_player_1.contains(1) && key_active_elements_player_1.contains(2) && key_active_elements_player_1.contains(3)) {
            won = true;
        }
        if (key_active_elements_player_1.contains(1) && key_active_elements_player_1.contains(4) && key_active_elements_player_1.contains(7)) {
            won = true;
        }
        if (key_active_elements_player_1.contains(7) && key_active_elements_player_1.contains(8) && key_active_elements_player_1.contains(9)) {
            won = true;
        }
        if (key_active_elements_player_1.contains(2) && key_active_elements_player_1.contains(5) && key_active_elements_player_1.contains(8)) {
            won = true;
        }
        if (key_active_elements_player_1.contains(3) && key_active_elements_player_1.contains(6) && key_active_elements_player_1.contains(9)) {
            won = true;
        }
        if (key_active_elements_player_1.contains(1) && key_active_elements_player_1.contains(5) && key_active_elements_player_1.contains(9)) {
            won = true;
        }
        if (key_active_elements_player_1.contains(3) && key_active_elements_player_1.contains(5) && key_active_elements_player_1.contains(7)) {
            won = true;
        }

        //get pressed buttons of player 2 and check if path leads to win
        if (key_active_elements_player_2.contains(1) && key_active_elements_player_2.contains(2) && key_active_elements_player_2.contains(3)) {
            won = true;
        }
        if (key_active_elements_player_2.contains(1) && key_active_elements_player_2.contains(4) && key_active_elements_player_2.contains(7)) {
            won = true;
        }
        if (key_active_elements_player_2.contains(7) && key_active_elements_player_2.contains(8) && key_active_elements_player_2.contains(9)) {
            won = true;
        }
        if (key_active_elements_player_2.contains(2) && key_active_elements_player_2.contains(5) && key_active_elements_player_2.contains(8)) {
            won = true;
        }
        if (key_active_elements_player_2.contains(3) && key_active_elements_player_2.contains(6) && key_active_elements_player_2.contains(9)) {
            won = true;
        }
        if (key_active_elements_player_2.contains(1) && key_active_elements_player_2.contains(5) && key_active_elements_player_2.contains(9)) {
            won = true;
        }
        if (key_active_elements_player_2.contains(3) && key_active_elements_player_2.contains(5) && key_active_elements_player_2.contains(7)) {
            won = true;
        }
        return won;
    }
}
