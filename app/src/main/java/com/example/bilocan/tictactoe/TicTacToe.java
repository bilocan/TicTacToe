package com.example.bilocan.tictactoe;

import android.widget.ImageView;

public class TicTacToe {
    private boolean isPressed;
    private ImageView imageView;
    private String ticType;
    private int board_number;

    public TicTacToe(ImageView imageView, int board_number) {
        this.imageView = imageView;
        this.board_number = board_number;
    }

    public int getBoard_number() {
        return board_number;
    }

    public void setBoard_number(int board_number) {
        this.board_number = board_number;
    }

    public String getTicType() {
        return ticType;
    }

    public void setTicType(String ticType) {
        this.ticType = ticType;
    }

    public boolean isPressed() {
        return isPressed;
    }

    public void setPressed(boolean pressed) {
        isPressed = pressed;
    }

    public ImageView getImageView() {
        return imageView;
    }

    public void setImageView(ImageView imageView) {
        this.imageView = imageView;
    }
}
