package com.example.bilocan.tictactoe;

import java.util.ArrayList;

public class AllTicTacToes {
    private static AllTicTacToes allTicTacToes_instance;
    private ArrayList<TicTacToe> ticTacToes = new ArrayList<>();

    private AllTicTacToes() {  }

    public static AllTicTacToes instance() {
        if(allTicTacToes_instance == null) {
            allTicTacToes_instance = new AllTicTacToes();
            return allTicTacToes_instance;
        } else {
            return allTicTacToes_instance;
        }
    }

    public void addTicTacToe(TicTacToe t) {
        ticTacToes.add(t);
    }

    public void delTicTacToe(TicTacToe t) {
        ticTacToes.remove(t);
    }

    public TicTacToe search(int id) {
        TicTacToe ticTacToe= null;
        for(TicTacToe t : ticTacToes) {
            if(t.getBoard_number() == id) {
                ticTacToe = t;
            }
        }
        return ticTacToe;
    }
}
